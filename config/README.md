# Configuration Module

This is configuration module in which it supports these file

  - Consul remote configuration
  - Environment variables
  - YAML file

The prefix for the configuration enviranment variable is `LENDER`. For example if we have configuration like this:

```yaml
---
port: 8089
env: DEV
postgres:
  master:
    host: master-postgres-host
    port: 5432
    schema: lenders
    dbName: lender
    user: amartha
    password: some-password 
  slave:
    host: slave-postgres-host
    port: 5432
    schema: lenders
    dbName: lender
    user: amartha-slave
    password: some-password-slave
feature:
  otp:
    enabled: false
```

The key for master host is `postgres.master.host`. The environment variable willbe `LENDER_POSTGRES_MASTER_HOST`.
Environment variable precedes the config file. So to override a configuration you can set do things like

```shell
$ export LENDER_POSTGRES_MASTER_DBNAME=lender-test-db
$ export LENDER_PORT=8089
$ export LENDER_FEATURE_OTP_ENABLED=1
```

Then run the binary, this will override the settings on the yaml file


## In the server

As this will run by systemd and unit file, the environment variable is specified in the `EnvironmentFile`, in which the
canonical location will be `/etc/sysconfig/legacy-wrapper-service`. The default file will be empty. To change the file you'd
need to SSH, edit the file, and restart the service using `systemctl restart legacy-wrapper-service`. 

