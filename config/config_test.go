package config

import (
	"os"
	"testing"
	"time"

	"github.com/icrowley/fake"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
)

var expectedConfig Config = Config{
	Port:                 8080,
	NotificationURL:      "https//notification.url.amartha.id:7890",
	OTPExpiryTime:        200 * time.Nanosecond,
	GoogleProjectID:      "amartha-dev",
	GoogleCredentialPath: "creds-pubsub.json",
	TokenSalt:            "token-salt",
	ConsumerConfig: ConsumerConfig{
		Enabled:            false,
		PrivyIDCallbackSub: "legacy-wrapper-service.privy-id-callback-consumer",
	},
	Redis: RedisConfig{
		Host: "redis-host",
		Port: 6666,
	},
	Env: EnvironmentDev,
	Postgres: PostgresConfig{
		MaxOpenConnections: 10,
		MaxIdleConnections: 5,
		Master: PSQL{
			Host:     "master-postgres-host",
			Port:     5432,
			Schema:   "lender",
			DBName:   "lenders",
			User:     "amartha",
			Password: "some-password",
		},
		Slave: PSQL{
			Host:     "slave-postgres-host",
			Port:     5433,
			Schema:   "lender",
			DBName:   "lenders",
			User:     "amartha-slave",
			Password: "some-password-slave",
		},
	},
	Feature: FeatureFlag{
		OTP: FeatureConfig{
			Enabled: false,
		},
	},
}

func TestConfigFromFile(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping test in short mode, this is an integration test")
	}

	v := viper.New()
	var c Config

	InitialiseDefaults(v)

	assert.NoError(t, InitialiseFileAndEnv(v, "config.test"))
	assert.NoError(t, v.Unmarshal(&c))

	assert.Equal(t, expectedConfig, c)
}

func TestConfigFromEnvironment(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping test in short mode, this is an integration test")
	}

	v := viper.New()
	var c Config

	os.Setenv("LENDER_POSTGRES_MASTER_HOST", "changed-master-host")
	os.Setenv("LENDER_POSTGRES_SLAVE_PORT", "6789")
	os.Setenv("LENDER_ENV", EnviromentUAT)
	os.Setenv("LENDER_FEATURE_OTP_ENABLED", "0")

	InitialiseDefaults(v)

	if assert.NoError(t, InitialiseFileAndEnv(v, "config.test")) {
		assert.NoError(t, v.Unmarshal(&c))

		assert.NotEqual(t, expectedConfig, c)
		assert.Equal(t, "changed-master-host", c.Postgres.Master.Host)
		assert.Equal(t, 6789, c.Postgres.Slave.Port)
		assert.Equal(t, EnviromentUAT, c.Env)
		assert.False(t, c.Feature.OTP.Enabled)
	}
}

func TestConfigFromRemote(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping test in short mode, this is an integration test")
	}

	v := viper.New()
	var c Config

	InitialiseDefaults(v)
	if assert.NoError(t, InitialiseRemote(v)) {
		assert.NoError(t, v.Unmarshal(&c))
		assert.Equal(t, expectedConfig, c)
	}
}

func TestRolloutPercentage(t *testing.T) {
	const loanIDWithinRollout = 2001002
	const loanIDOutsideRollout = 101199

	const rolloutPercentage = 50

	assert.True(t, IsWithinRolloutPercentage(loanIDWithinRollout, rolloutPercentage))
	assert.False(t, IsWithinRolloutPercentage(loanIDOutsideRollout, rolloutPercentage))
}

func TestConfigGetNotifSrvURL(t *testing.T) {
	t.Run("should return expected notification url", func(t *testing.T) {
		url := fake.IPv4()
		c := Config{
			NotificationURL: url,
		}
		notifURL := c.GetNotifSrvURL()
		assert.Equal(t, url, notifURL)
	})
}

func TestConfigGetOTPFlag(t *testing.T) {
	t.Run("should return correct otp feature flag", func(t *testing.T) {
		c := Config{
			Feature: FeatureFlag{
				OTP: FeatureConfig{
					Enabled: false,
				},
			},
		}
		t.Run("when otp feature flag is false", func(t *testing.T) {
			otpFlag := c.GetOTPFlag()
			assert.False(t, otpFlag)
		})
		t.Run("when otp feature flag is true", func(t *testing.T) {
			c.Feature.OTP.Enabled = true
			otpFlag := c.GetOTPFlag()
			assert.True(t, otpFlag)
		})
	})
}

func TestConfigGetEmailFlag(t *testing.T) {
	t.Run("should return correct email feature flag", func(t *testing.T) {
		c := Config{
			Feature: FeatureFlag{
				Email: FeatureConfig{
					Enabled: false,
				},
			},
		}
		t.Run("when email feature flag is false", func(t *testing.T) {
			emailFlag := c.GetEmailFlag()
			assert.False(t, emailFlag)
		})
		t.Run("when email feature flag is true", func(t *testing.T) {
			c.Feature.Email.Enabled = true
			emailFlag := c.GetEmailFlag()
			assert.True(t, emailFlag)
		})
	})
}

func TestGetNotifSrvURL(t *testing.T) {
	url := fake.IPv4()
	c := Config{
		NotificationURL: url,
	}
	gotURL := c.GetNotifSrvURL()
	assert.Equal(t, url, gotURL)
}

func TestGetBankSrvURL(t *testing.T) {
	url := fake.IPv4()
	c := Config{
		BankingURL: url,
	}
	gotURL := c.GetBankSrvURL()
	assert.Equal(t, url, gotURL)
}

func TestGetInvestorSrvURL(t *testing.T) {
	url := fake.IPv4()
	c := Config{
		GoInvestorURL: url,
	}
	gotURL := c.GetInvestorSrvURL()
	assert.Equal(t, url, gotURL)
}
