package config

var configDefaults = map[string]interface{}{
	"port":                        8080,
	"redis.host":                  "localhost",
	"redis.port":                  6379,
	"postgres.master.host":        "localhost",
	"postgres.master.port":        5432,
	"postgres.master.schema":      "lender",
	"postgres.master.dbName":      "lenders",
	"postgres.master.user":        "postgres",
	"postgres.slave.host":         "localhost",
	"postgres.slave.port":         5432,
	"postgres.slave.schema":       "lender",
	"postgres.slave.dbName":       "lenders",
	"postgres.slave.user":         "postgres",
	"postgres.maxOpenConnection":  25,
	"postgres.maxIdleConnection":  25,
	"feature.email.enabled":       false,
	"feature.otp.enabled":         false,
	"feature.privyId.enabled":     false,
	"consumer.enabled":            false,
	"consumer.privyIdCallbackSub": "legacy-wrapper-service.privy-id-callback-consumer",
	"googlePubSub.lenderTopic":    "lender",
}

const configName = "config"

var searchPath = []string{
	"/etc/amartha/legacy-wrapper-service",
	"$HOME/.legacy-wrapper-service",
	".",
}
