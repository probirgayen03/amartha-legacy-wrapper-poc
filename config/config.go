package config

import (
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	_ "github.com/spf13/viper/remote"
)

var mu sync.Mutex

var (
	EnvironmentDev  = "DEV"
	EnvironmentProd = "PROD"
	EnviromentUAT   = "UAT"
)

// Configuration : Global Config Instance
var Configuration Config

// Config for the srvice
type Config struct {
	Postgres             PostgresConfig `mapstructure:"postgres"`
	GooglePubSub         GooglePubSub   `mapstructure:"googlePubSub"`
	Env                  string         `mapstructure:"env"`
	Port                 int            `mapstructure:"port"`
	Redis                RedisConfig    `mapstructure:"redis"`
	NotificationURL      string         `mapstructure:"notificationURL"`
	PrivyIDURL           string         `mapstructure:"privyIdURL"`
	FileStorageURL       string         `mapstructure:"fileStorageURL"`
	OTPExpiryTime        time.Duration  `mapstructure:"otpExpiryTime"`
	TokenSecretKey       string         `mapstructure:"tokenSecretKey"`
	Feature              FeatureFlag    `mapstructure:"feature"`
	GoogleProjectID      string         `mapstructure:"googleProjectId"`
	GoogleCredentialPath string         `mapstructure:"googleCredentialPath"`
	ConsumerConfig       ConsumerConfig `mapstructure:"consumer"`
	GoInvestorURL        string         `mapstructure:"goInvestorURL"`
	BankingURL           string         `mapstructure:"bankingURL"`
	MisURL               string         `mapstructure:"misURL"`
	BankingSecretKey     string         `mapstructure:"bankingSecretKey"`
	TokenSalt            string         `mapstructure:"tokenSalt"`
	UpdateProfileURL     string         `mapstructure:"updateProfileUrl"`
}

// ConsumerConfig Information
type ConsumerConfig struct {
	Enabled            bool   `mapstructure:"enabled"`
	PrivyIDCallbackSub string `mapstructure:"privyIdCallbackSub"`
}

// GooglePubSub information
type GooglePubSub struct {
	ProjectID         string `mapstructure:"projectId"`
	OrderTopic        string `mapstructure:"orderTopic"`
	LoanSubscriber    string `mapstructure:"loanSubscriber"`
	AccountSubscriber string `mapstructure:"accountSubscriber"`
}

// PSQL config information.
type PSQL struct {
	Host     string `mapstructure:"host"`
	Port     int    `mapstructure:"port"`
	Schema   string `mapstructure:"schema"`
	DBName   string `mapstructure:"dbName"`
	User     string `mapstructure:"user"`
	Password string `mapstructure:"password"`
}

// PostgresConfig info
type PostgresConfig struct {
	ConnMaxLifetime    time.Duration `mapstructure:"connMaxLifetime"`
	MaxOpenConnections int           `mapstructure:"maxOpenConnections"`
	MaxIdleConnections int           `mapstructure:"maxIdleConnections"`
	Master             PSQL          `mapstructure:"master"`
	Slave              PSQL          `mapstructure:"slave"`
}

// RedisConfig info
type RedisConfig struct {
	Host string `mapstructure:"host"`
	Port int    `mapstructure:"port"`
}

// ConnectionStringWithSSLMode : db connection string
func (p PSQL) ConnectionStringWithSSLMode(sslmode string) string {
	return fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s search_path=%s sslmode=%s",
		p.Host, strconv.Itoa(p.Port), p.User, p.Password, p.DBName, p.Schema, sslmode)
}

// ConnectionString func
func (p PSQL) ConnectionString() string {
	return p.ConnectionStringWithSSLMode("disable")
}

func (r RedisConfig) Address() string {
	return fmt.Sprintf("%s:%d", r.Host, r.Port)
}

type FeatureConfig struct {
	Enabled           bool `mapstructure:"enabled"`
	RolloutPercentage int  `mapstructure:"rolloutPercentage"`
}

func IsWithinRolloutPercentage(identifier int, rolloutPercentage int) bool {
	return identifier%100 < rolloutPercentage
}

type FeatureFlag struct {
	OTP     FeatureConfig `mapstructure:"otp"`
	Email   FeatureConfig `mapstructure:"email"`
	PrivyID FeatureConfig `mapstructure:"privyId"`
}

func (c Config) GetNotifSrvURL() string {
	return c.NotificationURL
}

func (c Config) GetFileSrvURL() string {
	return c.FileStorageURL
}

func (c Config) GetOTPFlag() bool {
	return c.Feature.OTP.Enabled
}

func (c Config) GetEmailFlag() bool {
	return c.Feature.Email.Enabled
}

func (c Config) GetBankSrvURL() string {
	return c.BankingURL
}

func (c Config) GetInvestorSrvURL() string {
	return c.GoInvestorURL
}

func initialiseRemote(v *viper.Viper) error {
	_ = v.AddRemoteProvider("consul", "localhost:8500", "GO_CORE_ORDER")
	v.SetConfigType("yaml")
	return v.ReadRemoteConfig()
}

func initialiseFileAndEnv(v *viper.Viper, configName string) error {
	v.SetConfigName(configName)

	for _, path := range searchPath {
		v.AddConfigPath(path)
	}

	v.SetEnvPrefix("ORDER")
	v.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	v.AutomaticEnv()

	return v.ReadInConfig()
}

func initialiseDefaults(v *viper.Viper) {
	for key, value := range configDefaults {
		v.SetDefault(key, value)
	}
}

func loadConfig() {
	v := viper.New()
	initialiseDefaults(v)
	if err := initialiseRemote(v); err != nil {
		log.Warningf("No remote server configured will load configuration from file and environment variables: %+v", err)
		if err := initialiseFileAndEnv(v, configName); err != nil {
			if _, ok := err.(viper.ConfigFileNotFoundError); ok {
				log.Warning("No 'config.yaml' file found on search paths. Will either use environment variables or defaults")
			} else {
				log.Fatalf("Error occurred during loading config: %s", err.Error())
			}
		}
	}
	err := v.Unmarshal(&Configuration)
	if err != nil {
		log.WithFields(log.Fields{
			"file": "config",
			"func": "loadConfig",
		}).Fatal("Error un-marshalling configuration")
	}
}

// Initialise : config initialisung method
func Initialise() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGHUP)
	go func() {
		for {
			<-c
			log.Infof("Got SIGHUP Signal, Reloading config...")
			loadConfig()
		}
	}()
	loadConfig()
}
