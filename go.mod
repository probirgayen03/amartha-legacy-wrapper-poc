module bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc

go 1.14

require (
	cloud.google.com/go/pubsub v1.3.1
	github.com/DATA-DOG/go-sqlmock v1.4.1
	github.com/corpix/uarand v0.1.1 // indirect
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/gin-gonic/gin v1.6.2
	github.com/golang/mock v1.4.3
	github.com/google/uuid v1.1.1
	github.com/icrowley/fake v0.0.0-20180203215853-4178557ae428
	github.com/jinzhu/gorm v1.9.12
	github.com/pelletier/go-toml v1.7.0 // indirect
	github.com/rs/cors v1.7.0
	github.com/sirupsen/logrus v1.5.0
	github.com/spf13/viper v1.6.2
	github.com/stretchr/testify v1.4.0
	golang.org/x/sys v0.0.0-20200413165638-669c56c373c4 // indirect
	golang.org/x/tools v0.0.0-20200417140056-c07e33ef3290 // indirect
)
