package utils

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

// SuccessResponse - stored information for success response
type SuccessResponse struct {
	Status     string      `json:"status"`
	StatusCode int         `json:"statuscode"`
	Message    string      `json:"message"`
	Data       interface{} `json:"data"`
	Mapping    interface{} `json:"mapping"`
}

// ErrorResponse - stored information for success response
type ErrorResponse struct {
	Status     string      `json:"status"`
	StatusCode int         `json:"statuscode"`
	Message    string      `json:"message"`
	Error      interface{} `json:"data"`
}

// SetErrorResponse - set all properties error response
func SetErrorResponse(status string, statusCode int, message string, err interface{}) ErrorResponse {
	errResp := ErrorResponse{
		Status:     status,
		StatusCode: statusCode,
		Message:    message,
		Error:      err,
	}
	return errResp
}

// SetSuccessResponse - set all properties success response
func SetSuccessResponse(status string, statusCode int, message string, data interface{}) SuccessResponse {
	fmt.Print(data)
	succResp := SuccessResponse{
		Status:     status,
		StatusCode: statusCode,
		Message:    message,
		Data:       data,
	}

	mapping, ok := data.(map[string]interface{})
	if ok {
		succResp = SuccessResponse{
			Status:     status,
			StatusCode: statusCode,
			Message:    message,
			Data:       mapping["data"],
			Mapping:    mapping["mapping"],
		}
	}
	return succResp
}

// WriteSuccessResponse - write success response to the client
func WriteSuccessResponse(c *gin.Context, resp SuccessResponse) {
	c.JSON(resp.StatusCode, resp)
}

// WriteErrorResponse - write error response to the client
func WriteErrorResponse(c *gin.Context, resp ErrorResponse) {
	c.JSON(resp.StatusCode, resp)
}
