package investormodel

import (
	"time"
)

//Investor - data structure for amartha investor
type Investor struct {
	ID                       uint64     `gorm:"primary_key" gorm:"column:_id" json:"_id"`
	IsInstitutional          bool       `gorm:"column:isInstitutional" json:"isInstitutional"`
	IsCheckedTerm            bool       `gorm:"column:isCheckedTerm" json:"isCheckedTerm"`
	IsCheckedPrivacy         bool       `gorm:"column:isCheckedPrivacy" json:"isCheckedPrivacy"`
	IsAccountVerified        bool       `gorm:"column:isAccountVerified" json:"isAccountVerified"`
	InvestorNo               uint64     `gorm:"column:investorNo" json:"investorNo"`
	BankName                 string     `gorm:"column:bankName" json:"bankName"`
	BankBranch               string     `gorm:"column:bankBranch" json:"bankBranch"`
	BankAccountName          string     `gorm:"column:bankAccountName" json:"bankAccountName"`
	BankSwiftCode            string     `gorm:"column:bankSwiftCode" json:"bankSwiftCode"`
	BankAccountNo            string     `gorm:"column:bankAccountNo" json:"bankAccountNo"`
	ReferralCode             string     `gorm:"column:referralCode" json:"referralCode"`
	HeirName                 string     `gorm:"column:heirName" json:"heirName"`
	HeirIdCardNo             string     `gorm:"column:heirIdCardNo" json:"heirIdCardNo"`
	HeirPhone                string     `gorm:"column:heirPhone" json:"heirPhone"`
	HeirEmail                string     `gorm:"column:heirEmail" json:"heirEmail"`
	ReferrerUrl              string     `gorm:"column:referrerUrl" json:"referrerUrl"`
	HeirIdCardFilename       string     `gorm:"column:heirIdCardFilename" json:"heirIdCardFilename"`
	TempPrivyDoc             string     `gorm:"column:privyDocument" json:"-"`
	PrivyDoc                 string     `gorm:"column:privyDocument" json:"privyDocument"`
	RDLAccountName           string     `gorm:"column:rdlAccountName" json:"rdlAccountName"`
	RDLAccountNo             string     `gorm:"column:rdlAccountNo" json:"rdlAccountNo"`
	IsAutoPurchased          bool       `gorm:"column:isAutoPurchased" json:"isAutoPurchased"`
	IsAutoPurchasedInsurance bool       `gorm:"column:isAutoPurchasedInsurance" json:"isAutoPurchasedInsurance"`
	AutoPurchasedAt          *time.Time `gorm:"column:autoPurchasedAt" json:"autoPurchasedAt"`
	IsAutoGrow               bool       `gorm:"column:isAutoGrow" json:"isAutoGrow"`
	AutoGrowAt               *time.Time `gorm:"column:autoGrowAt" json:"autoGrowAt"`
	PrivySentAt              *time.Time `gorm:"column:privySentAt" json:"privySentAt"`
	RejectPreVerifiedDate    *time.Time `gorm:"column:rejectPreVerifiedDate" json:"-"`
	CreatedAt                time.Time  `gorm:"column:createdAt" json:"-"`
	UpdatedAt                time.Time  `gorm:"column:updatedAt" json:"-"`
	DeletedAt                *time.Time `gorm:"column:deletedAt" json:"-"`
}
