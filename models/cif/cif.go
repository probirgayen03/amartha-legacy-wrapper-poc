package cifmodel

import (
	"time"
)

//Cif - Amartha Customer Identification File
type Cif struct {
	ID                  uint64     `gorm:"primary_key" gorm:"column:_id" json:"_id"`
	CifNumber           uint64     `gorm:"column:cifNumber" json:"cifNumber"`
	Username            string     `gorm:"column:username" json:"username"`
	Password            string     `gorm:"column:password" json:"-"`
	Name                string     `gorm:"column:name" json:"name"`
	Gender              string     `gorm:"column:gender" json:"gender"`
	PlaceOfBirth        string     `gorm:"column:placeOfBirth" json:"placeOfBirth"`
	DateOfBirth         time.Time  `gorm:"column:dateOfBirth" json:"dateOfBirth"`
	IDCardNo            string     `gorm:"column:idCardNo" json:"idCardNo"`
	IDCardValidDate     string     `gorm:"column:idCardValidDate" json:"idCardValidDate"`
	IDCardFilename      string     `gorm:"column:idCardFilename" json:"idCardFilename"`
	TaxCardNo           string     `gorm:"column:taxCardNo" json:"taxCardNo"`
	TaxCardFilename     string     `gorm:"column:taxCardFilename" json:"taxCardFilename"`
	MaritalStatus       string     `gorm:"column:maritalStatus" json:"maritalStatus"`
	MotherName          string     `gorm:"column:mothersName" json:"mothersName"`
	Religion            string     `gorm:"column:religion" json:"religion"`
	Address             string     `gorm:"column:address" json:"address"`
	Kelurahan           string     `gorm:"column:kelurahan" json:"kelurahan"`
	Kecamatan           string     `gorm:"column:kecamatan" json:"kecamatan"`
	City                string     `gorm:"column:city" json:"city"`
	Province            string     `gorm:"column:province" json:"province"`
	Nationality         string     `gorm:"column:nationality" json:"nationality"`
	Zipcode             string     `gorm:"column:zipcode" json:"zipcode"`
	PhoneNo             string     `gorm:"column:phoneNo" json:"phoneNo"`
	CompanyName         string     `gorm:"column:companyName" json:"companyName"`
	CompanyAddress      string     `gorm:"column:companyAddress" json:"companyAddress"`
	Occupation          string     `gorm:"column:occupation" json:"occupation"`
	Income              float64    `gorm:"column:income" json:"income"`
	IncomeSourceFund    string     `gorm:"column:incomeSourceFund" json:"incomeSourceFund"`
	IncomeSourceCountry string     `gorm:"column:incomeSourceCountry" json:"incomeSourceCountry"`
	IsActivated         bool       `gorm:"column:isActivated" json:"isActivated"`
	IsVAlidated         bool       `gorm:"column:isValidated" json:"isValidated"`
	IsVerified          bool       `gorm:"column:isVerified" json:"isVerified"`
	IsDeclined          bool       `gorm:"column:isDeclined" json:"isDeclined"`
	VerificationNumber  string     `gorm:"column:verificationNumber" json:"-"`
	VerificationCode    string     `gorm:"column:verificationCode" json:"-"`
	SelfieFilename      string     `gorm:"column:selfieFilename" json:"selfieFilename"`
	PrivyUsername       string     `gorm:"column:privyUsername" json:"privyUsername"`
	PrivyUserToken      string     `gorm:"column:privyUserToken" json:"privyUserToken"`
	CreatedAt           time.Time  `gorm:"column:createdAt" json:"-"`
	UpdatedAt           time.Time  `gorm:"column:updatedAt" json:"-"`
	DeletedAt           *time.Time `gorm:"column:deletedAt" json:"-"`
}

type CIFPayload struct {
	Password            string    `gorm:"column:password" json:"password"`
	Name                string    `gorm:"column:name" json:"name"`
	Gender              string    `gorm:"column:gender" json:"gender"`
	PlaceOfBirth        string    `gorm:"column:placeOfBirth" json:"placeOfBirth"`
	DateOfBirth         time.Time `gorm:"column:dateOfBirth" json:"dateOfBirth"`
	IDCardNo            string    `gorm:"column:idCardNo" json:"idCardNo"`
	IDCardValidDate     string    `gorm:"column:idCardValidDate" json:"idCardValidDate"`
	IDCardFilename      string    `gorm:"column:idCardFilename" json:"idCardFilename"`
	TaxCardNo           string    `gorm:"column:taxCardNo" json:"taxCardNo"`
	TaxCardFilename     string    `gorm:"column:taxCardFilename" json:"taxCardFilename"`
	MaritalStatus       string    `gorm:"column:maritalStatus" json:"maritalStatus"`
	MotherName          string    `gorm:"column:mothersName" json:"mothersName"`
	Religion            string    `gorm:"column:religion" json:"religion"`
	Address             string    `gorm:"column:address" json:"address"`
	Kelurahan           string    `gorm:"column:kelurahan" json:"kelurahan"`
	Kecamatan           string    `gorm:"column:kecamatan" json:"kecamatan"`
	City                string    `gorm:"column:city" json:"city"`
	Province            string    `gorm:"column:province" json:"province"`
	Nationality         string    `gorm:"column:nationality" json:"nationality"`
	Zipcode             string    `gorm:"column:zipcode" json:"zipcode"`
	PhoneNo             string    `gorm:"column:phoneNo" json:"phoneNo"`
	CompanyName         string    `gorm:"column:companyName" json:"companyName"`
	CompanyAddress      string    `gorm:"column:companyAddress" json:"companyAddress"`
	Occupation          string    `gorm:"column:occupation" json:"occupation"`
	Income              float64   `gorm:"column:income" json:"income"`
	IncomeSourceFund    string    `gorm:"column:incomeSourceFund" json:"incomeSourceFund"`
	IncomeSourceCountry string    `gorm:"column:incomeSourceCountry" json:"incomeSourceCountry"`
	SelfieFilename      string    `gorm:"column:selfieFilename" json:"selfieFilename"`
}

//CifUpdatePayload - payload data structure for update cif data
type CifUpdatePayload struct {
	Base64File   string `json:"base64File,omitempty"`   // payload for upload
	IsFor        string `json:"isFor,omitempty"`        // payload for upload
	TargetFolder string `json:"targetFolder,omitempty"` // payload for upload
	CIFPayload
}

// UploadFile - payload data for Upload File
type UploadFile struct {
	Base64File   string `json:"base64File,omitempty"`   // payload for upload
	IsFor        string `json:"isFor,omitempty"`        // payload for upload
	TargetFolder string `json:"targetFolder,omitempty"` // payload for upload
}

// CifRejectReason -
type CifRejectReason struct {
	ID        uint64     `gorm:"column:id" json:"id"`
	CifID     uint64     `gorm:"column:cifId" json:"cifId"`
	Type      string     `gorm:"column:type" json:"type"`
	Data      string     `gorm:"column:data" json:"data"`
	CreatedAt time.Time  `gorm:"column:createdAt" json:"createdAt"`
	UpdatedAt time.Time  `gorm:"column:updatedAt" json:"updatedAt"`
	DeletedAt *time.Time `gorm:"column:deletedAt" json:"deletedAt"`
}
