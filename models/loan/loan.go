package loanmodel

import "time"

// Loan : Loan table
type Loan struct {
	ID                   uint64     `gorm:"primary_key" gorm:"column:_id" json:"_id"`
	Purpose              string     `gorm:"column:purpose" json:"purpose"`
	URLPic1              string     `gorm:"column:urlPic1" json:"urlPic1"`
	URLPic2              string     `gorm:"column:urlPic2" json:"urlPic2"`
	SubmittedLoanDate    *time.Time `gorm:"column:submittedLoanDate" json:"submittedLoanDate"`
	SubmittedPlafond     float64    `gorm:"column:submittedPlafond" json:"submittedPlafond"`
	SubmittedTenor       int64      `gorm:"column:submittedTenor" json:"submittedTenor"`
	SubmittedInstallment float64    `gorm:"column:submittedInstallment" json:"submittedInstallment"`
	CreditScoreGrade     string     `gorm:"column:creditScoreGrade" json:"creditScoreGrade"`
	CreditScoreValue     float64    `gorm:"column:creditScoreValue" json:"creditScoreValue"`
	Tenor                uint64     `gorm:"column:tenor" json:"tenor"`
	Rate                 float64    `gorm:"column:rate" json:"rate"`
	Installment          float64    `gorm:"column:installment" json:"installment"`
	Plafond              float64    `gorm:"column:plafond" json:"plafond"`
	Stage                string     `gorm:"column:stage" json:"stage"`
	InvestorID           uint64     `gorm:"column:stage" json:"investorId"`
	CreatedAt            time.Time  `gorm:"column:createdAt" json:"-"`
	UpdatedAt            time.Time  `gorm:"column:updatedAt" json:"-"`
	DeletedAt            *time.Time `gorm:"column:deletedAt" json:"-"`
}

// LoanHistory : Loan history info
type LoanHistory struct {
	ID        uint64     `gorm:"primary_key" gorm:"column:_id" json:"_id"`
	StageFrom string     `gorm:"column:stageFrom" json:"stageFrom"`
	StageTo   string     `gorm:"column:stageTo" json:"stageTo"`
	Remark    string     `gorm:"column:remark" json:"remark"`
	CreatedAt time.Time  `gorm:"column:createdAt" json:"createdAt"`
	UpdatedAt time.Time  `gorm:"column:updatedAt" json:"updatedAt"`
	DeletedAt *time.Time `gorm:"column:deletedAt" json:"deletedAt"`
}

//RLoanHistory - Relation 'loan' to `history`
type RLoanHistory struct {
	ID            uint64     `gorm:"primary_key" gorm:"column:_id" json:"_id"`
	LoanID        uint64     `gorm:"column:loanId" json:"loanId"`
	LoanHistoryID uint64     `gorm:"column:loanHistoryId" json:"loanHistoryId"`
	CreatedAt     time.Time  `gorm:"column:createdAt" json:"createdAt"`
	UpdatedAt     time.Time  `gorm:"column:updatedAt" json:"updatedAt"`
	DeletedAt     *time.Time `gorm:"column:deletedAt" json:"deletedAt"`
}
