package main

import (
	"os"

	db "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/bootstrap/db"
	queue "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/bootstrap/queue"
	routes "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/bootstrap/routes"
	log "github.com/sirupsen/logrus"
)

func main() {
	defer func() {
		log.Fatal(db.MasterDB.Close())
		log.Fatal(db.SlaveDB.Close())
	}()
	go queue.Start()
	port := os.Getenv("PORT")
	router := routes.Routes()

	if port != "" {
		router.Run(port)
	} else {
		router.Run(":8080")
	}
}
