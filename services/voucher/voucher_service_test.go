package voucherservice

import (
	"errors"
	"reflect"
	"testing"
	"time"

	"github.com/golang/mock/gomock"

	voucherModel "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/models/voucher"
	voucherRepository "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/repositories/sql/voucher"
	voucherRepositoryMock "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/repositories/sql/voucher/mock"
)

var (
	gomockCtrl      *gomock.Controller
	voucherRepoMock *voucherRepositoryMock.MockIRepository
)

func TestMain(m *testing.M) {
	m.Run()
}

func beforeTest(t *testing.T) {
	gomockCtrl = gomock.NewController(t)
	voucherRepoMock = voucherRepositoryMock.NewMockIRepository(gomockCtrl)
}

func Test_GetService(t *testing.T) {
	beforeTest(t)
	type args struct {
		voucherRepo voucherRepository.IRepository
	}
	testCases := []struct {
		name string
		args args
		want IService
	}{
		{"Get Service Interface", args{voucherRepo: voucherRepoMock}, &Service{VoucherRepo: voucherRepoMock}},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			if servive := GetService(testCase.args.voucherRepo); !reflect.DeepEqual(servive, testCase.want) {
				t.Errorf("GetService() = %v, want %v", servive, testCase.want)
			}
		})
	}
}

func Test_ValidateVoucher(t *testing.T) {
	beforeTest(t)

	var (
		validLenderID        = "12345"
		validVoucherCode     = "VALIDVCODE"
		errorVoucherCode     = "ERRORVCODE"
		invalidVoucherCode   = "INVALIDVCODE"
		futureVoucherCode    = "FUTUREVCODE"
		expiredVoucherCode   = "EXPIREDVCODE"
		errorValidCountCode  = "ERRORVALIDVCOUNTCODE"
		usedValidVoucherCode = "USEDVALIDVCODE"
	)

	timeNow := time.Now()
	validVoucher := voucherModel.Voucher{
		ID:          111,
		Amount:      1000,
		VoucherNo:   "VALIDVOUCHER",
		Description: "test voucher",
		IsPersonal:  false,
		Remark:      "Valid Voucher",
		StartDate:   timeNow.AddDate(0, 0, -7),
		EndDate:     timeNow.AddDate(0, 0, 7),
		CreatedAt:   timeNow,
		UpdatedAt:   timeNow,
	}
	futureVoucher := validVoucher
	futureVoucher.StartDate = timeNow.AddDate(0, 0, 3)
	expiredVoucher := validVoucher
	expiredVoucher.EndDate = timeNow.AddDate(0, 0, -3)

	voucherRepoMock.EXPECT().GetVoucher(errorVoucherCode).Return(voucherModel.Voucher{}, errors.New(""))
	voucherRepoMock.EXPECT().GetVoucher(invalidVoucherCode).Return(voucherModel.Voucher{}, nil)
	voucherRepoMock.EXPECT().GetVoucher(futureVoucherCode).Return(futureVoucher, nil)
	voucherRepoMock.EXPECT().GetVoucher(expiredVoucherCode).Return(expiredVoucher, nil)

	voucherRepoMock.EXPECT().GetVoucher(errorValidCountCode).Return(validVoucher, nil)
	voucherRepoMock.EXPECT().GetVoucherUsageCount(validLenderID, errorValidCountCode).Return(int64(0), errors.New(""))

	voucherRepoMock.EXPECT().GetVoucher(usedValidVoucherCode).Return(validVoucher, nil)
	voucherRepoMock.EXPECT().GetVoucherUsageCount(validLenderID, usedValidVoucherCode).Return(int64(5), nil)

	voucherRepoMock.EXPECT().GetVoucher(validVoucherCode).Return(validVoucher, nil)
	voucherRepoMock.EXPECT().GetVoucherUsageCount(validLenderID, validVoucherCode).Return(int64(0), nil)

	type args struct {
		customerID  string
		voucherCode string
	}

	//--------------------- SUCCESS TEST CASES ---------------------------------

	type SuccessTestCases struct {
		name string
		args args
		want voucherModel.Voucher
	}

	vService := Service{VoucherRepo: voucherRepoMock}
	successTestCases := []SuccessTestCases{
		{"valid voucher code", args{validLenderID, validVoucherCode}, validVoucher},
	}

	for _, testCase := range successTestCases {
		t.Run(testCase.name, func(t *testing.T) {
			voucher, _ := vService.ValidateVoucher(testCase.args.customerID, testCase.args.voucherCode)
			if !reflect.DeepEqual(voucher, testCase.want) {
				t.Errorf("ValidateVoucher() = %v, want %v", voucher, testCase.want)
			}
		})
	}

	//--------------------- ERROR TEST CASES -----------------------------------

	type ErrorTestCases struct {
		name string
		args args
		want error
	}

	errorTestCases := []ErrorTestCases{
		{"DB error in GetVoucher call", args{validLenderID, errorVoucherCode}, errors.New("Kode voucher tidak valid")},
		{"Invalid Voucher code", args{validLenderID, invalidVoucherCode}, errors.New("Kode voucher tidak valid")},
		{"future Voucher code", args{validLenderID, futureVoucherCode}, errors.New("Masa berlaku kode voucher telah berakhir")},
		{"expired Voucher code", args{validLenderID, expiredVoucherCode}, errors.New("Masa berlaku kode voucher telah berakhir")},
		{"DB error in GetVoucherUsageCount call", args{validLenderID, errorValidCountCode}, errors.New("Kode voucher tidak valid")},
		{"used voucher code", args{validLenderID, usedValidVoucherCode}, errors.New("Anda sudah pernah melakukan transaksi dengan menggunakan kode voucher tersebut")},
	}

	for _, testCase := range errorTestCases {
		t.Run(testCase.name, func(t *testing.T) {
			_, err := vService.ValidateVoucher(testCase.args.customerID, testCase.args.voucherCode)
			if !reflect.DeepEqual(err, testCase.want) {
				t.Errorf("ValidateVoucher() = %v, want %v", err, testCase.want)
			}
		})
	}
}
