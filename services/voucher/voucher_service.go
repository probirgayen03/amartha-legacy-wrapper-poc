package voucherservice

import (
	"errors"
	"time"

	voucherModel "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/models/voucher"
	voucherRepo "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/repositories/sql/voucher"
	log "github.com/sirupsen/logrus"
)

type IService interface {
	ValidateVoucher(customerID string, voucherCode string) (voucherModel.Voucher, error)
}

type Service struct {
	VoucherRepo voucherRepo.IRepository
}

func GetService(vRepo voucherRepo.IRepository) IService {
	return &Service{
		VoucherRepo: vRepo,
	}
}

func (service Service) ValidateVoucher(customerID string, voucherCode string) (voucherModel.Voucher, error) {
	var voucher voucherModel.Voucher
	voucher, err := service.VoucherRepo.GetVoucher(voucherCode)

	if err != nil {
		log.WithFields(log.Fields{"file": "voucher_service.go", "func": "ValidateVoucher"}).
			Error("error while validating voucher")
		return voucher, errors.New("Kode voucher tidak valid")
	}

	if voucher == (voucherModel.Voucher{}) {
		log.WithFields(log.Fields{"file": "voucher_service.go", "func": "ValidateVoucher"}).
			Error("unable to find voucher")
		return voucher, errors.New("Kode voucher tidak valid")
	}

	timeNow := time.Now()
	if timeNow.Before(voucher.StartDate) {
		log.WithFields(log.Fields{"file": "voucher_service.go", "func": "ValidateVoucher"}).
			Error("Voucher not available")
		return voucher, errors.New("Masa berlaku kode voucher telah berakhir")
	}

	if timeNow.After(voucher.EndDate) {
		log.WithFields(log.Fields{"file": "voucher_service.go", "func": "ValidateVoucher"}).
			Error("Voucher Expired")
		return voucher, errors.New("Masa berlaku kode voucher telah berakhir")
	}

	vUseCount, err := service.VoucherRepo.GetVoucherUsageCount(customerID, voucherCode)

	if err != nil {
		log.WithFields(log.Fields{"file": "voucher_service.go", "func": "ValidateVoucher"}).
			Error("error while validating voucher")
		return voucher, errors.New("Kode voucher tidak valid")
	}

	if vUseCount > 0 {
		log.WithFields(log.Fields{"file": "voucher_service.go", "func": "ValidateVoucher"}).
			Error("Voucher Expired")
		return voucher, errors.New("Anda sudah pernah melakukan transaksi dengan menggunakan kode voucher tersebut")
	} else {

		if voucher.Remark == "First" {
			// TODO: check first time voucher need to implemented. seems the query wrong

			// `select count(l.id) from investor i
			// left join loan l on l."investorId" = i.id
			// where i."deletedAt" isnull and l."deletedAt" isnull
			// and stage in ('INSTALLMENT','END','END-EARLY','INVESTOR')
			// and i.id = ?`

			// if  loanCount != 0 {
			// 	log.WithFields(log.Fields{"file": "voucher_service.go", "func": "ValidateVoucher"}).
			// 		Error("Voucher is not using first time")
			// 	return nil, errors.New("Kode voucher tidak valid")
			// }
		}
	}
	return voucher, nil
}
