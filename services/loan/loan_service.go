package loanservice

import (
	publisher "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/bootstrap/queue/publisher"
	config "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/config"
	constants "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/constants/queue_constants"
	investorModel "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/models/investor"
	loanModel "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/models/loan"
	investorRepo "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/repositories/sql/investor"
	loanRepo "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/repositories/sql/loan"
)

// IService ...
type IService interface {
	ChangeLoanStage(loanID string, investorID string, stage string) loanModel.Loan
}

// Service ...
type Service struct {
	InvestorRepository investorRepo.IRepository
	LoanRepository     loanRepo.IRepository
}

// NewService ...
func NewService(loanRepository loanRepo.IRepository, investorRepository investorRepo.IRepository) IService {
	return &Service{
		InvestorRepository: investorRepository,
		LoanRepository:     loanRepository,
	}
}

// ChangeLoanStage ...
func (service Service) ChangeLoanStage(loanID string, investorID string, stage string) loanModel.Loan {
	topic := config.Configuration.GooglePubSub.OrderTopic
	eventName := constants.BLOCK_LOAN_RES
	investor := service.InvestorRepository.GetInvestorDetails(investorID)

	if investor == (investorModel.Investor{}) {
		publisher.PublishMsg(topic, eventName, map[string]string{"loanID": loanID, "investorID": investorID, "stage": stage, "status": "Failed"})
		return loanModel.Loan{}
	}

	loan := service.LoanRepository.UpdateLoanStage(loanID, investorID, stage)
	publisher.PublishMsg(topic, eventName, map[string]string{"loanID": loanID, "investorID": investorID, "stage": stage, "status": "Success"})
	return loan
}
