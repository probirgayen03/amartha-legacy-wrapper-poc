package loanservice

import (
	"reflect"
	"testing"

	investorModel "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/models/investor"
	loanModel "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/models/loan"
	investorRepoMock "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/repositories/sql/investor/mock"
	loanRepoMock "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/repositories/sql/loan/mock"
	"github.com/golang/mock/gomock"
)

func TestService_ChangeLoanStage(t *testing.T) {
	ctrl := gomock.NewController(t)
	investorRepositoryMock := investorRepoMock.NewMockIRepository(ctrl)
	loanRepositoryMock := loanRepoMock.NewMockIRepository(ctrl)
	loanServiceMock := NewService(loanRepositoryMock, investorRepositoryMock)

	investorRepositoryMock.EXPECT().GetInvestorDetails("1234").Return(investorModel.Investor{ID: 1234})
	investorRepositoryMock.EXPECT().GetInvestorDetails("12345").Return(investorModel.Investor{})
	loanRepositoryMock.EXPECT().UpdateLoanStage("123", "1234", "CART").Return(loanModel.Loan{ID: 123, Stage: "CART", InvestorID: 1234}).Times(1)

	type args struct {
		loanID     string
		investorID string
		stage      string
	}
	tests := []struct {
		name string
		args args
		want loanModel.Loan
	}{
		{"Valid loan id", args{"123", "1234", "CART"}, loanModel.Loan{ID: 123, Stage: "CART", InvestorID: 1234}},
		{"Valid loan id", args{"123", "12345", "CART"}, loanModel.Loan{}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := loanServiceMock.ChangeLoanStage(tt.args.loanID, tt.args.investorID, tt.args.stage); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Service.ChangeLoanStage() = %v, want %v", got, tt.want)
			}
		})
	}
}
