package investorservice

import (
	"errors"

	// cifModel "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/models/cif"
	investorModel "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/models/investor"
	cifRepo "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/repositories/sql/cif"
	investorRepo "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/repositories/sql/investor"
	"github.com/gin-gonic/gin"
	// "github.com/jinzhu/gorm"
	// log "github.com/sirupsen/logrus"
)

type IService interface {
	InvestorDetails(investorID string) investorModel.Investor
	InvestorVerificationStatus(investorID string) (interface{}, error)
}

type Service struct {
	CifRepository      cifRepo.IRepository
	InvestorRepository investorRepo.IRepository
}

func NewService(investorRepository investorRepo.IRepository, cifRepository cifRepo.IRepository) IService {
	return &Service{
		InvestorRepository: investorRepository,
		CifRepository:      cifRepository,
	}
}

// InvestorDetails ...
func (service Service) InvestorDetails(investorID string) investorModel.Investor {
	investorDetail := service.InvestorRepository.GetInvestorDetails(investorID)

	if (investorModel.Investor{}) == investorDetail {
		return investorDetail
	}
	return investorDetail
}

// InvestorVerificationStatus ...
func (service Service) InvestorVerificationStatus(investorID string) (interface{}, error) {
	status, err := service.CifRepository.IsInvestorVerified(investorID)

	if err != nil {
		return nil, errors.New("Invalid Investor ID")
	}

	responsePayload := gin.H{
		"status":     status,
		"investorId": investorID,
	}
	return responsePayload, nil
}
