package investorservice

import (
	"fmt"
	"reflect"
	"testing"

	investorModel "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/models/investor"
	cifRepoMock "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/repositories/sql/cif/mock"
	investorRepoMock "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/repositories/sql/investor/mock"
	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
)

func TestService_InvestorDetails(t *testing.T) {
	ctrl := gomock.NewController(t)
	investorRepositoryMock := investorRepoMock.NewMockIRepository(ctrl)
	cifRepositoryMock := cifRepoMock.NewMockIRepository(ctrl)
	investorServiceMock := NewService(investorRepositoryMock, cifRepositoryMock)

	investorRepositoryMock.EXPECT().GetInvestorDetails("123").Return(investorModel.Investor{ID: 123}).Times(1)
	investorRepositoryMock.EXPECT().GetInvestorDetails("1234").Return(investorModel.Investor{}).Times(1)

	type args struct {
		investorID string
	}
	tests := []struct {
		name string
		args args
		want investorModel.Investor
	}{
		{"Run valid investor ID case", args{"123"}, investorModel.Investor{ID: 123}},
		{"Run invalid investor ID case", args{"1234"}, investorModel.Investor{}},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := investorServiceMock.InvestorDetails(tt.args.investorID); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Service.InvestorDetails() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestService_InvestorVerificationStatus(t *testing.T) {
	ctrl := gomock.NewController(t)
	investorRepositoryMock := investorRepoMock.NewMockIRepository(ctrl)
	cifRepositoryMock := cifRepoMock.NewMockIRepository(ctrl)
	investorServiceMock := NewService(investorRepositoryMock, cifRepositoryMock)

	cifRepositoryMock.EXPECT().IsInvestorVerified("123").Return(true, nil).Times(1)
	cifRepositoryMock.EXPECT().IsInvestorVerified("1234").Return(false, fmt.Errorf("Invalid ID")).Times(1)

	type args struct {
		investorID string
	}

	tests := []struct {
		name    string
		args    args
		want    interface{}
		wantErr bool
	}{
		{"Verify Valid ID", args{"123"}, gin.H{"investorId": "123", "status": true}, false},
		{"Verify Valid ID", args{"1234"}, nil, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := investorServiceMock.InvestorVerificationStatus(tt.args.investorID)
			if (err != nil) != tt.wantErr {
				t.Errorf("Service.InvestorVerificationStatus() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Service.InvestorVerificationStatus() = %v, want %v", got, tt.want)
			}
		})
	}
}
