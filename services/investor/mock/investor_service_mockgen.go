// Code generated by MockGen. DO NOT EDIT.
// Source: services/investor/investor_service.go

// Package mock is a generated GoMock package.
package mock

import (
	investormodel "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/models/investor"
	gomock "github.com/golang/mock/gomock"
	reflect "reflect"
)

// MockIService is a mock of IService interface
type MockIService struct {
	ctrl     *gomock.Controller
	recorder *MockIServiceMockRecorder
}

// MockIServiceMockRecorder is the mock recorder for MockIService
type MockIServiceMockRecorder struct {
	mock *MockIService
}

// NewMockIService creates a new mock instance
func NewMockIService(ctrl *gomock.Controller) *MockIService {
	mock := &MockIService{ctrl: ctrl}
	mock.recorder = &MockIServiceMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockIService) EXPECT() *MockIServiceMockRecorder {
	return m.recorder
}

// InvestorDetails mocks base method
func (m *MockIService) InvestorDetails(investorID string) investormodel.Investor {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "InvestorDetails", investorID)
	ret0, _ := ret[0].(investormodel.Investor)
	return ret0
}

// InvestorDetails indicates an expected call of InvestorDetails
func (mr *MockIServiceMockRecorder) InvestorDetails(investorID interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "InvestorDetails", reflect.TypeOf((*MockIService)(nil).InvestorDetails), investorID)
}

// InvestorVerificationStatus mocks base method
func (m *MockIService) InvestorVerificationStatus(investorID string) (interface{}, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "InvestorVerificationStatus", investorID)
	ret0, _ := ret[0].(interface{})
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// InvestorVerificationStatus indicates an expected call of InvestorVerificationStatus
func (mr *MockIServiceMockRecorder) InvestorVerificationStatus(investorID interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "InvestorVerificationStatus", reflect.TypeOf((*MockIService)(nil).InvestorVerificationStatus), investorID)
}
