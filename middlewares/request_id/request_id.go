package requestloggermiddleware

import (
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// RequestID ...
func RequestID() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		requestID := ctx.Request.Header.Get("X-Request-Id")

		// Create request id with UUID4
		if requestID == "" {
			uuid4, _ := uuid.NewUUID()
			requestID = uuid4.String()
		}

		// Expose it for use in the application
		ctx.Set("RequestId", requestID)

		// Set X-Request-Id header
		ctx.Writer.Header().Set("X-Request-Id", requestID)
		ctx.Next()
	}
}
