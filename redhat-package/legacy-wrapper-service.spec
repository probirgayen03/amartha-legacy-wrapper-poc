Name: legacy-wrapper-service
Version: %{_version}
Release: 1%{?dist}
Summary: legacy-wrapper-service

License: Proprietary
BuildArch: x86_64
BuildRequires: systemd
Requires(pre): shadow-utils

%global output_dir /usr/local/amartha/bin

%pre
getent group %{name} >/dev/null || groupadd -r %{name}
getent passwd %{name} >/dev/null || \
    useradd -r -g %{name} -d /home/%{name} -m -c "User for the '%{name}' service" %{name}
exit 0
mkdir -p %{buildroot}%{output_dir}/

%description
This is the service for managing lenders.

%install
mkdir -p %{buildroot}%{output_dir}/

%{__install} -D -m 0755 $RPM_SOURCE_DIR/%{name} %{buildroot}%{output_dir}
%{__install} -D -m 0644 $RPM_SOURCE_DIR/%{name}.service %{buildroot}%{_unitdir}/%{name}.service

%files
%{output_dir}/%{name}
%{_unitdir}/%{name}.service

%post
systemctl daemon-reload
systemctl enable %{name}.service
systemctl start %{name}.service

%preun
if [ $1 == 0 ]; then #uninstall
  systemctl unmask %{name}.service
  systemctl stop %{name}.service
  systemctl disable %{name}.service
fi

%postun
if [ $1 == 0 ]; then #uninstall
  systemctl daemon-reload
  systemctl reset-failed
fi

%changelog
* Fri Dec 14 2019 Didiet Noor<rms.hpd@amartha.com>
- First Build
