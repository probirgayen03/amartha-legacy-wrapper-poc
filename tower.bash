#!/bin/sh -ex

# host = Tower host
# username = Tower username
# password = Tower user password
# ID = ID of job template being launched.

echo "Configuring Tower Settings"
tower-cli config verify_ssl false
tower-cli config host $tower_server
userval=$(tower-cli config username $tower_user)
passwordval=$(tower-cli config password $tower_pass)
version=$(cat ./version.txt)
downloadurl=$(cat ./self-signed-url.txt)
consulconfigfile=$(cat config.${environment}.yaml | base64 -w 0)

if [[ $userval == "username: " ]] || [[ $passwordval == "password: " ]]
then
  echo "-- WARNING: Configuration has not been fully set -";
  echo "---- You will want to run the $ tower-cli config ";
  echo "---- command for host, username, and password ";
fi

# Let's run a tower-cli job
ID=$(tower-cli job_template list --name $tower_job_name -f id)
tower-cli job launch --job-template $ID --monitor --extra-vars="go_service_version=$version" --extra-vars="target_hosts=$deploy_to_hosts" --extra-vars="download_url=$downloadurl" --extra-vars="consul_service_key=$consul_service_key" --extra-vars="consul_config_file=$consulconfigfile" --extra-vars="vault_token=$vault_login_token" --extra-vars="service_exec_and_user=$service_executable_and_user"
