package pubsubqueue

import (
	"context"

	publisher "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/bootstrap/queue/publisher"
	subscriber "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/bootstrap/queue/subscriber"
	"bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/config"
	"cloud.google.com/go/pubsub"
	log "github.com/sirupsen/logrus"
)

var Client *pubsub.Client

func init() {
	ctx := context.Background()
	project := config.Configuration.GooglePubSub.ProjectID

	var err error
	log.Info("Initializing queue client")

	Client, err = pubsub.NewClient(ctx, project)
	if err != nil {

		log.WithFields(log.Fields{
			"file": "queue_subscription",
			"func": "init",
		}).Fatalf("Could not create pubsub Client: %v", err)
	} else {

		log.Info("%q Queue client initiated", project)
	}
	subscriber.CreateEventDelegatorMap()
}

func Start() {
	// Initialize all topics
	topicIdList := []string{
		config.Configuration.GooglePubSub.OrderTopic,
	}

	for _, topicID := range topicIdList {
		go publisher.StartTopic(Client, topicID)
	}

	// Initialize all subscriptions and keep them watching
	subscriptionList := []string{
		config.Configuration.GooglePubSub.AccountSubscriber,
		config.Configuration.GooglePubSub.LoanSubscriber,
	}

	for _, subscriptionID := range subscriptionList {
		go subscriber.StartSubscription(Client, subscriptionID)
	}
}
