package pubsubqueuesubscriber

import (
	container "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/bootstrap/service_container"
	constants "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/constants/queue_constants"
	"cloud.google.com/go/pubsub"
	log "github.com/sirupsen/logrus"
)

var eventDelegatorMap = make(map[string]func(*pubsub.Message))

func dispatchEventDelegator(eventName string) func(*pubsub.Message) {
	eventHandler := eventDelegatorMap[eventName]
	if eventHandler == nil {
		return fallbackEventHandler
	}
	return eventDelegatorMap[eventName]
}

// CreateEventDelegatorMap : Create event to delegator mapper
func CreateEventDelegatorMap() {
	loanController := container.ServiceContainer().InjectLoanController()
	eventDelegatorMap[constants.BLOCK_LOAN] = loanController.UpdateLoanStage
}

func fallbackEventHandler(msg *pubsub.Message) {
	log.WithFields(log.Fields{
		"event": msg.Data,
	}).Error("Event handler not found")
}
