package pubsubqueuesubscriber

import (
	"context"

	"cloud.google.com/go/pubsub"
	log "github.com/sirupsen/logrus"
)

func StartSubscription(client *pubsub.Client, subscription string) {
	if err := pullMsgs(client, subscription); err != nil {
		log.WithFields(log.Fields{
			"file":         "queue_subscription",
			"func":         "StartSubscription",
			"subscription": subscription,
		}).Fatal(err)
	} else {
		log.Info(subscription + " subscription up")
	}
}

func pullMsgs(client *pubsub.Client, subscription string) error {
	ctx := context.Background()
	//var mu sync.Mutex
	sub := client.Subscription(subscription)
	cancelCtx, cancel := context.WithCancel(ctx)
	err := sub.Receive(cancelCtx, func(ctx context.Context, msg *pubsub.Message) {
		eventName := string(msg.Data)
		log.WithFields(log.Fields{
			"subscription": subscription,
			"message": eventName,
		}).Info("Received message")
		dispatchEventDelegator(eventName)(msg)
	})
	defer func() {
		log.WithFields(log.Fields{
			"file": "queue_subscription",
			"func": "pullMsgs",
		}).Error("Exiting subscription " + subscription)
		cancel()
	}()
	return err
}
