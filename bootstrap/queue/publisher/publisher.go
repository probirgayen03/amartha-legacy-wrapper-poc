package pubsubqueuepublisher

import (
	"context"

	"cloud.google.com/go/pubsub"
)

type PublishReturn struct {
	status string
	msg    string
	id     string
}

var topicsMap = make(map[string]*pubsub.Topic)

func StartTopic(client *pubsub.Client, topicID string) {
	topicsMap[topicID] = client.Topic(topicID)
}

func PublishMsg(topicName, msg string, attributes map[string]string) *PublishReturn {
	ctx := context.Background()
	if topic, available := topicsMap[topicName]; available {
		// Publish the message
		result := topic.Publish(ctx, &pubsub.Message{Data: []byte(msg), Attributes: attributes})

		// Wait for the server generated ID for the published msg
		msgId, err := result.Get(ctx)

		if err != nil {
			return &PublishReturn{status: "Failure", msg: "Google pub/sub message publish failed"}
		}
		return &PublishReturn{status: "Success", msg: "Message published successfully", id: msgId}
	}
	return &PublishReturn{status: "Failure", msg: "Requested topic is not available"}
}
