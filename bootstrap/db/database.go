package db

import (
	"os"

	"bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/config"
	"github.com/jinzhu/gorm"

	// postgres
	_ "github.com/jinzhu/gorm/dialects/postgres"
	log "github.com/sirupsen/logrus"
)

// DB Instances ...
var (
	MasterDB *gorm.DB
	SlaveDB  *gorm.DB
)

func init() {
	config.Initialise()
	var err error
	log.Info("Initializing Master DB")
	log.Info(config.Configuration.Postgres.Master)
	MasterDB, err = InitialiseDB(config.Configuration.Postgres.Master.ConnectionString())
	if err != nil {
		log.WithFields(log.Fields{
			"file": "database.go",
			"func": "init",
			"db":   "master",
		}).Fatal(err)
	}
	log.Info("Initializing Slave DB")
	SlaveDB, err = InitialiseDB(config.Configuration.Postgres.Slave.ConnectionString())
	if err != nil {
		log.WithFields(log.Fields{
			"file": "database.go",
			"func": "init",
			"db":   "slave",
		}).Fatal(err)
	}
}

// InitialiseDB : DB intialization
func InitialiseDB(connectionString string) (*gorm.DB, error) {
	db, err := gorm.Open("postgres", connectionString)

	if err != nil {
		return &gorm.DB{}, err
	}

	if os.Getenv("ENV") == "development" {
		db = db.Debug()
	}

	db.DB().SetMaxIdleConns(config.Configuration.Postgres.MaxIdleConnections)
	db.DB().SetMaxOpenConns(config.Configuration.Postgres.MaxOpenConnections)
	db.DB().SetConnMaxLifetime(config.Configuration.Postgres.ConnMaxLifetime)
	return db, nil
}
