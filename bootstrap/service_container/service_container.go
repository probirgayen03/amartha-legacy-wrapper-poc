package servicecontainer

import (
	"sync"

	db "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/bootstrap/db"
	investorCtrl "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/controllers/investor"
	loanCtrl "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/controllers/loan"
	cifRepository "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/repositories/sql/cif"
	investorRepository "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/repositories/sql/investor"
	loanRepository "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/repositories/sql/loan"
	investorService "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/services/investor"

	voucherCtrl "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/controllers/voucher"
	voucherRepository "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/repositories/sql/voucher"
	loanService "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/services/loan"
	voucherService "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/services/voucher"
)

// IServiceContainer ...
type IServiceContainer interface {
	InjectInvestorController() investorCtrl.IController
	InjectVoucherController() voucherCtrl.IController
	InjectLoanController() loanCtrl.IController
}

type kernel struct{}

func (k *kernel) InjectInvestorController() investorCtrl.IController {
	investorRepository := investorRepository.NewRepository(db.MasterDB, db.SlaveDB)
	cifRepository := cifRepository.NewRepository(db.MasterDB, db.SlaveDB)
	investorService := investorService.NewService(investorRepository, cifRepository)
	investorController := investorCtrl.NewController(investorService)

	return investorController
}

func (k *kernel) InjectVoucherController() voucherCtrl.IController {
	vRepository := voucherRepository.GetRepository(db.MasterDB, db.SlaveDB)
	vService := voucherService.GetService(vRepository)
	vController := voucherCtrl.GetVoucherController(vService)

	return vController
}

func (k *kernel) InjectLoanController() loanCtrl.IController {
	loanRepository := loanRepository.NewRepository(db.MasterDB, db.SlaveDB)
	investorRepository := investorRepository.NewRepository(db.MasterDB, db.SlaveDB)
	loanService := loanService.NewService(loanRepository, investorRepository)
	loanController := loanCtrl.NewController(loanService)

	return loanController
}

var (
	k             *kernel
	containerOnce sync.Once
)

// ServiceContainer ...
func ServiceContainer() IServiceContainer {
	if k == nil {
		containerOnce.Do(func() {
			k = &kernel{}
		})
	}
	return k
}
