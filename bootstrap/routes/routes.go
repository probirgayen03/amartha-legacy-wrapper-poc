package bootstraprouter

import (
	"github.com/gin-gonic/gin"

	config "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/config"
	requestIDMiddleware "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/middlewares/request_id"
	cors "github.com/rs/cors/wrapper/gin"
	log "github.com/sirupsen/logrus"
)

// Routes ...
// @version 1.0.0
// @description This function has all the routes ifo
// @BasePath bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/
func Routes() *gin.Engine {
	log.Info("Router initializing")
	router := gin.Default()
	router.Use(requestIDMiddleware.RequestID())
	router.Use(cors.New(cors.Options{
		Debug: config.Configuration.Env == "development",
	}))

	router.GET("/status", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"status": "UP",
		})
	})

	InvestorRouter(router)
	VoucherRouter(router)
	log.Info("Router initialized")
	return router
}
