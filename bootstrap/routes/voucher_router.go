package bootstraprouter

import (
	container "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/bootstrap/service_container"
	"github.com/gin-gonic/gin"
)

// VoucherRouter ...
// @title wrapper service
// @version 1.0
// @description This is a sample API.
// @contact.name API Support
// @host localhost
// @BasePath /
func VoucherRouter(router *gin.Engine) {
	voucherController := container.ServiceContainer().InjectVoucherController()

	v1Group := router.Group("/v1")
	{
		voucher := v1Group.Group("voucher")
		{
			voucher.GET("/validate", voucherController.ValidateVoucher)
		}
	}
}
