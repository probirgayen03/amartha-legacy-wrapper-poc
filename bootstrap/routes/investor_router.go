package bootstraprouter

import (
	container "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/bootstrap/service_container"
	"github.com/gin-gonic/gin"
)

// InvestorRouter ...
// @title wrapper service
// @version 1.0
// @description This is a sample API.
// @contact.name API Support
// @host localhost
// @BasePath /
func InvestorRouter(router *gin.Engine) {
	investorController := container.ServiceContainer().InjectInvestorController()

	v1Group := router.Group("/v1")
	{
		investor := v1Group.Group("investor")
		{
			investor.GET("/:investorid", investorController.GetLenderInfo)
			investor.GET("/:investorid/status", investorController.GetLenderStatus)
		}
	}
}
