package investorcontroller

import (
	"math/rand"
	"net/http"

	investorService "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/services/investor"
	response "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/utils"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

type IController interface {
	GetLenderInfo(ctx *gin.Context)
	GetLenderStatus(ctx *gin.Context)
}

type Controller struct {
	InvestorService investorService.IService
}

func NewController(investorService investorService.IService) IController {
	return &Controller{
		InvestorService: investorService,
	}
}

// GetLenderInfo ...
func (ctrl Controller) GetLenderInfo(ctx *gin.Context) {

	var investorId = ctx.Param("investorid")

	if len(investorId) < 1 {
		log.WithFields(log.Fields{
			"file": "investor_controller",
			"func": "GetLenderInfo",
		})

		var returnData = response.SetErrorResponse("failure", http.StatusBadRequest, "invalid or empty data provided", struct{}{})
		response.WriteErrorResponse(ctx, returnData)
		// ctx.JSON(http.StatusUnprocessableEntity, returnData)
		return
	}
	// we will add stub code for mimic the responce

	randValue := rand.Intn(4)

	if randValue > 2 {
		response.WriteSuccessResponse(ctx, response.SetSuccessResponse("success", http.StatusOK, "record found", struct{}{}))
		return
	}

	// actual code should be written here

	response.WriteSuccessResponse(ctx, response.SetSuccessResponse("failure", http.StatusOK, "record not found", struct{}{}))
	return
}

// GetLenderStatus
func (ctrl Controller) GetLenderStatus(ctx *gin.Context) {
	var investorID = ctx.Param("investorid")

	if len(investorID) < 1 {
		log.WithFields(log.Fields{
			"file": "investor_controller",
			"func": "GetLenderInfo",
		})

		var returnData = response.SetErrorResponse("failure", http.StatusBadRequest, "invalid or empty data provided", struct{}{})
		response.WriteErrorResponse(ctx, returnData)
		return
	}

	res, err := ctrl.InvestorService.InvestorVerificationStatus(investorID)
	if err != nil {
		response.WriteErrorResponse(ctx, response.SetErrorResponse("error", http.StatusBadRequest, err.Error(), res))
		return
	}
	response.WriteSuccessResponse(ctx, response.SetSuccessResponse("success", http.StatusOK, "", res))
	return
}
