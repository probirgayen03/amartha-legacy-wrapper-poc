package loancontroller

import (
	// "math/rand"
	// "net/http"

	"fmt"

	loanService "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/services/loan"
	"cloud.google.com/go/pubsub"
	// response "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/utils"
	// "github.com/gin-gonic/gin"
	// log "github.com/sirupsen/logrus"
)

// IController ...
type IController interface {
	UpdateLoanStage(msg *pubsub.Message)
}

// Controller ...
type Controller struct {
	LoanService loanService.IService
}

// NewController ...
func NewController(loanService loanService.IService) IController {
	return &Controller{
		LoanService: loanService,
	}
}

// UpdateLoanStage ...
func (ctrl Controller) UpdateLoanStage(msg *pubsub.Message) {
	fmt.Print(msg.Attributes)
	stage := msg.Attributes["stage"]
	loanID := msg.Attributes["loanID"]
	investorID := msg.Attributes["investorID"]
	loan := ctrl.LoanService.ChangeLoanStage(loanID, investorID, stage)

	if loan.Stage == stage {
		fmt.Print("Ack")
		msg.Ack()
	}
}
