package loancontroller

import (
	"reflect"
	"testing"

	loanService "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/services/loan"
	"cloud.google.com/go/pubsub"
)

func TestNewController(t *testing.T) {
	type args struct {
		loanService loanService.IService
	}
	tests := []struct {
		name string
		args args
		want IController
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewController(tt.args.loanService); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewController() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestController_UpdateLoanStage(t *testing.T) {
	type fields struct {
		LoanService loanService.IService
	}
	type args struct {
		msg *pubsub.Message
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := Controller{
				LoanService: tt.fields.LoanService,
			}
			ctrl.UpdateLoanStage(tt.args.msg)
		})
	}
}
