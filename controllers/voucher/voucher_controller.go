package vouchercontroller

import (
	"net/http"

	voucherModel "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/models/voucher"
	voucherService "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/services/voucher"
	response "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/utils"
	"github.com/gin-gonic/gin"
)

type IController interface {
	ValidateVoucher(ctx *gin.Context)
}

type Controller struct {
	VoucherService voucherService.IService
}

func GetVoucherController(vService voucherService.IService) IController {
	return &Controller{
		VoucherService: vService,
	}
}

type FormattedVoucherData struct {
	ID          uint64  `json:"id"`
	Amount      float64 `json:"amount"`
	VoucherNo   string  `json:"voucherNo"`
	Description string  `gorm:"column:description" json:"description"`
}

func (ctrl Controller) ValidateVoucher(ctx *gin.Context) {
	customerID := ctx.Query("customerId")
	voucherCode := ctx.Query("code")

	if customerID == "" || voucherCode == "" {
		errResp := response.SetErrorResponse("failure", http.StatusBadRequest, "invalid or empty data provided", struct{}{})
		ctx.JSON(http.StatusBadRequest, errResp)
		return
	}

	var voucher voucherModel.Voucher
	voucher, err := ctrl.VoucherService.ValidateVoucher(customerID, voucherCode)

	if err != nil {
		errResp := response.SetErrorResponse("failure", http.StatusBadRequest, err.Error(), struct{}{})
		ctx.JSON(http.StatusBadRequest, errResp)
		return
	}

	formattedData := FormattedVoucherData{voucher.ID, voucher.Amount, voucher.VoucherNo, voucher.Description}
	var msg = "Selamat anda berhak mendapatkan potongan investasi"
	succRes := response.SetSuccessResponse("success", http.StatusOK, msg, formattedData)
	ctx.JSON(http.StatusOK, succRes)
	return
}
