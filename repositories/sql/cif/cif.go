package cifrepository

import (
	"github.com/jinzhu/gorm"
	log "github.com/sirupsen/logrus"
	// cifModel "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/models/cif"
)

var tableName string = "cif"

type repo struct {
	MasterDB *gorm.DB
	SlaveDB  *gorm.DB
}

type IRepository interface {
	IsInvestorVerified(investorId string) (bool, error)
}

func NewRepository(masterDB *gorm.DB, slaveDB *gorm.DB) IRepository {
	return &repo{
		MasterDB: masterDB,
		SlaveDB:  slaveDB,
	}
}

// IsInvestorVerified ...
func (repo repo) IsInvestorVerified(investorID string) (bool, error) {
	var cifRes struct {
		IsVerified bool `gorm:"column:isVerified" json:"isVerified"`
	}
	err := repo.SlaveDB.Table(tableName).Select(`cif."isVerified"`).Joins(`join r_cif_investor on r_cif_investor."cifId" = cif.id`).Where(`r_cif_investor."investorId" = ?`, investorID).Find(&cifRes).Error
	if err != nil {
		log.WithFields(log.Fields{
			"file": "cif.go",
			"func": "IsInvestorVerified",
		}).Error(err.Error())
		return false, err
	}
	return cifRes.IsVerified, nil
}
