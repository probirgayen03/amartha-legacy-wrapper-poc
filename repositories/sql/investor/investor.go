package investorrepository

import (
	"fmt"
	"strconv"

	investorModel "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/models/investor"
	"github.com/jinzhu/gorm"
	log "github.com/sirupsen/logrus"
)

var tableName string = "investor"

type repo struct {
	MasterDB *gorm.DB
	SlaveDB  *gorm.DB
}

func NewRepository(masterDB *gorm.DB, slaveDB *gorm.DB) IRepository {
	return &repo{
		MasterDB: masterDB,
		SlaveDB:  slaveDB,
	}
}

type IRepository interface {
	GetInvestorDetails(investorId string) investorModel.Investor
}

func (repo repo) GetInvestorDetails(investorID string) investorModel.Investor {
	var item investorModel.Investor
	id, err := strconv.ParseUint(investorID, 10, 64)
	if err != nil {
		log.WithFields(log.Fields{
			"fileName": "investor.go",
			"func":     "GetInvestorDetails",
		}).Error("Unable to parse investor ID")
		return investorModel.Investor{}
	}
	err = repo.SlaveDB.Table(tableName).Where("id = ?", id).Find(&item).Error
	fmt.Print(item.ID)
	fmt.Print("\n new line")
	if err != nil {
		log.WithFields(log.Fields{
			"fileName": "investor.go",
			"func":     "GetInvestorDetails",
		}).Error("Invalid investor id")
		return investorModel.Investor{}
	}
	return item
}
