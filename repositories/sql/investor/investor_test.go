package investorrepository

import (
	"database/sql"
	"reflect"
	"regexp"
	"testing"

	investorModel "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/models/investor"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jinzhu/gorm"
)

var (
	masterMock sqlmock.Sqlmock
	master     *sql.DB
	slaveMock  sqlmock.Sqlmock
	slave      *sql.DB
	masterDB   *gorm.DB
	slaveDB    *gorm.DB
	err        error
)

func TestMain(m *testing.M) {
	setup()
	m.Run()
	teardown()
}

func setup() {
	master, masterMock, err = sqlmock.New()
	masterDB, err = gorm.Open("postgres", master)
	masterDB.LogMode(false)
	slave, slaveMock, err = sqlmock.New()
	slaveDB, err = gorm.Open("postgres", slave)
	slaveDB.LogMode(false)
}

func teardown() {
	defer masterDB.Close()
	defer slaveDB.Close()
}

func TestNewRepository(t *testing.T) {
	type args struct {
		masterDB *gorm.DB
		slaveDB  *gorm.DB
	}
	tests := []struct {
		name string
		args args
		want IRepository
	}{
		{"Get Repository Interface", args{masterDB: masterDB, slaveDB: slaveDB}, repo{MasterDB: masterDB, SlaveDB: slaveDB}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewRepository(tt.args.masterDB, tt.args.slaveDB); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewRepository() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_repo_GetInvestorDetails(t *testing.T) {
	slaveMock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "` + tableName)).
		WillReturnRows(sqlmock.NewRows([]string{"id", "investorNo", "bankName"}).
			AddRow(123, 1234, "test Bank"))
	type fields struct {
		MasterDB *gorm.DB
		SlaveDB  *gorm.DB
	}
	type args struct {
		investorID string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   investorModel.Investor
	}{
		{"Valid Investor ID", fields{MasterDB: masterDB, SlaveDB: slaveDB}, args{investorID: "123"}, investorModel.Investor{ID: 123, InvestorNo: 1234, BankName: "test Bank"}},
		{"Invalid Investor ID", fields{MasterDB: masterDB, SlaveDB: slaveDB}, args{investorID: "12"}, investorModel.Investor{}},
		{"Invalid Investor ID Type", fields{MasterDB: masterDB, SlaveDB: slaveDB}, args{investorID: "12A"}, investorModel.Investor{}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := repo{
				MasterDB: tt.fields.MasterDB,
				SlaveDB:  tt.fields.SlaveDB,
			}
			if got := repo.GetInvestorDetails(tt.args.investorID); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("repo.GetInvestorDetails() = %v, want %v", got, tt.want)
			}
		})
	}
}
