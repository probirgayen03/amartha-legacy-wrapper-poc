package loanrepository

import (
	"strconv"

	loanModel "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/models/loan"
	"github.com/jinzhu/gorm"
	log "github.com/sirupsen/logrus"
)

var tableName string = "loan"

type repo struct {
	MasterDB *gorm.DB
	SlaveDB  *gorm.DB
}

// IRepository :  Loan Repository interface
type IRepository interface {
	UpdateLoanStage(loanID string, investorID string, stage string) loanModel.Loan
}

// NewRepository : New Loan Repository instance
func NewRepository(masterDB *gorm.DB, slaveDB *gorm.DB) IRepository {
	return &repo{
		MasterDB: masterDB,
		SlaveDB:  slaveDB,
	}
}

// UpdateLoanStage : Update loan stage and investor id
func (repo repo) UpdateLoanStage(loanID string, investorID string, stage string) loanModel.Loan {
	var loan loanModel.Loan
	id, err := strconv.ParseUint(loanID, 10, 64)
	invID, err1 := strconv.ParseUint(investorID, 10, 64)

	if err != nil || err1 != nil {
		log.WithFields(log.Fields{
			"fileName": "loan.go",
			"func":     "UpdateLoanStage",
		}).Error("Unable to parse loan or Investor ID")
		return loanModel.Loan{}
	}

	err = repo.MasterDB.Table(tableName).Where("id = ?", id).First(&loan).Error

	if err != nil {
		log.WithFields(log.Fields{
			"fileName": "loan.go",
			"func":     "UpdateLoanStage",
		}).Error("Invalid loanID")
		return loanModel.Loan{}
	}

	loan.Stage = stage
	loan.InvestorID = invID
	// repo.MasterDB.Save(&loan)
	return loan
}
