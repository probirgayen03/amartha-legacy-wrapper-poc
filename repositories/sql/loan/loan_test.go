package loanrepository

import (
	"database/sql"
	"reflect"
	"regexp"
	"testing"

	loanModel "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/models/loan"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jinzhu/gorm"
)

var (
	masterMock sqlmock.Sqlmock
	master     *sql.DB
	slaveMock  sqlmock.Sqlmock
	slave      *sql.DB
	masterDB   *gorm.DB
	slaveDB    *gorm.DB
	err        error
)

func TestMain(m *testing.M) {
	setup()
	m.Run()
	teardown()
}

func setup() {
	slave, slaveMock, err = sqlmock.New()
	slaveDB, err = gorm.Open("postgres", slave)
	slaveDB.LogMode(false)
	master, masterMock, err = sqlmock.New()
	masterDB, err = gorm.Open("postgres", master)
	masterDB.LogMode(false)
}

func teardown() {
	defer masterDB.Close()
	defer slaveDB.Close()
}
func TestNewRepository(t *testing.T) {
	type args struct {
		masterDB *gorm.DB
		slaveDB  *gorm.DB
	}
	tests := []struct {
		name string
		args args
		want IRepository
	}{
		{"Get Repository Interface", args{masterDB: masterDB, slaveDB: slaveDB}, repo{MasterDB: masterDB, SlaveDB: slaveDB}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewRepository(tt.args.masterDB, tt.args.slaveDB); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewRepository() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_repo_UpdateLoanStage(t *testing.T) {
	masterMock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "` + tableName)).
		WithArgs(123).
		WillReturnRows(sqlmock.NewRows([]string{"id", "investorId", "stage"}).
			AddRow(123, 1234, ""))
	type fields struct {
		MasterDB *gorm.DB
		SlaveDB  *gorm.DB
	}
	type args struct {
		loanID     string
		investorID string
		stage      string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   loanModel.Loan
	}{
		{"Valid Investor ID", fields{MasterDB: masterDB, SlaveDB: slaveDB}, args{"123", "1234", "CART"}, loanModel.Loan{ID: 123, InvestorID: 1234, Stage: "CART"}},
		{"Invalid Investor ID", fields{MasterDB: masterDB, SlaveDB: slaveDB}, args{"12", "123", "CART"}, loanModel.Loan{}},
		{"Invalid Investor ID type", fields{MasterDB: masterDB, SlaveDB: slaveDB}, args{"12A", "123B", "CART"}, loanModel.Loan{}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := repo{
				MasterDB: tt.fields.MasterDB,
				SlaveDB:  tt.fields.SlaveDB,
			}
			if got := repo.UpdateLoanStage(tt.args.loanID, tt.args.investorID, tt.args.stage); !reflect.DeepEqual(got, tt.want) {
				t.Fatalf("repo.UpdateLoanStage() = %v, want %v", got, tt.want)
			}
		})
	}
}
