package voucher

import (
	"database/sql"
	"reflect"
	"regexp"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jinzhu/gorm"

	voucherModel "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/models/voucher"
)

var (
	masterMock sqlmock.Sqlmock
	master     *sql.DB
	slaveMock  sqlmock.Sqlmock
	slave      *sql.DB
	masterDB   *gorm.DB
	slaveDB    *gorm.DB
	err        error
)

func TestMain(m *testing.M) {
	setup()
	m.Run()
	teardown()
}

func setup() {
	slave, slaveMock, err = sqlmock.New()
	slaveDB, err = gorm.Open("postgres", slave)
	slaveDB.LogMode(false)
	master, masterMock, err = sqlmock.New()
	masterDB, err = gorm.Open("postgres", master)
	masterDB.LogMode(false)
}

func teardown() {
	masterDB.Close()
	slaveDB.Close()
}

func Test_GetRepository(t *testing.T) {
	type args struct {
		masterDB *gorm.DB
		slaveDB  *gorm.DB
	}
	testCases := []struct {
		name string
		args args
		want IRepository
	}{
		{"Get Repository Interface", args{masterDB: masterDB, slaveDB: slaveDB}, &Repository{MasterDB: masterDB, SlaveDB: slaveDB}},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			if repo := GetRepository(testCase.args.masterDB, testCase.args.slaveDB); !reflect.DeepEqual(repo, testCase.want) {
				t.Errorf("GetRepository() = %v, want %v", repo, testCase.want)
			}
		})
	}
}

func Test_GetVoucher(t *testing.T) {
	timeNow := time.Now()

	var (
		validVoucherCode   = "VTESTSUCCESS"
		invalidVoucherCode = "VTESTFAIL"
	)

	slaveMock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "voucher" WHERE "voucher"."deletedAt" IS NULL AND (("voucherNo" = $1)) ORDER BY "voucher"."id" ASC LIMIT 1`)).
		WithArgs(validVoucherCode).
		WillReturnRows(sqlmock.NewRows([]string{"id", "amount", "voucherNo", "description", "isPersonal", "remark", "startDate", "endDate", "createdAt", "updatedAt", "deletedAt"}).
			AddRow(111, 1000, validVoucherCode, "test voucher", false, "test", timeNow, timeNow, timeNow, timeNow, timeNow))

	slaveMock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "voucher" WHERE "voucher"."deletedAt" IS NULL AND (("voucherNo" = $1)) ORDER BY "voucher"."id" ASC LIMIT 1`)).
		WithArgs(invalidVoucherCode).
		WillReturnRows(sqlmock.NewRows([]string{"id", "amount", "voucherNo", "description", "isPersonal", "remark", "startDate", "endDate", "createdAt", "updatedAt", "deletedAt"}))

	type args struct {
		voucherCode string
	}

	testCases := []struct {
		name string
		args args
		want voucherModel.Voucher
	}{
		{"valid Voucher", args{validVoucherCode}, voucherModel.Voucher{ID: 111, Amount: 1000, VoucherNo: validVoucherCode, Description: "test voucher", Remark: "test", IsPersonal: false, StartDate: timeNow, EndDate: timeNow, CreatedAt: timeNow, UpdatedAt: timeNow, DeletedAt: timeNow}},
		{"invalid Voucher", args{invalidVoucherCode}, voucherModel.Voucher{}},
	}

	repo := Repository{MasterDB: masterDB, SlaveDB: slaveDB}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			voucher, _ := repo.GetVoucher(testCase.args.voucherCode)
			if !reflect.DeepEqual(voucher, testCase.want) {
				t.Fatalf("repo.UpdateLoanStage() = %v, want %v", voucher, testCase.want)
			}
		})
	}
}

func Test_GetVoucherUsageCount(t *testing.T) {
	var (
		validLenderID      = "12345"
		inValidLenderID    = "#asd"
		validVoucherCode   = "VTESTSUCCESS"
		invalidVoucherCode = "VTESTFAIL"
	)

	slaveMock.ExpectQuery(regexp.QuoteMeta(
		`SELECT count(*) FROM "voucher" JOIN r_loan_order_voucher as rlov ON rlov."voucherId" = voucher.id JOIN r_loan_order as rlo ON rlo."loanOrderId" = rlov."loanOrderId" JOIN loan ON loan.id = rlo."loanId" WHERE (voucher."voucherNo" = $1 and loan."investorId" = $2) AND (rlov.deleteAt isnull and voucher.deleteAt isnull) AND (loan.deleteAt isnull and rlo.deleteAt isnull)`)).
		WithArgs(validVoucherCode, validLenderID).
		WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(1))

	slaveMock.ExpectQuery(regexp.QuoteMeta(
		`SELECT count(*) FROM "voucher" JOIN r_loan_order_voucher as rlov ON rlov."voucherId" = voucher.id JOIN r_loan_order as rlo ON rlo."loanOrderId" = rlov."loanOrderId" JOIN loan ON loan.id = rlo."loanId" WHERE (voucher."voucherNo" = $1 and loan."investorId" = $2) AND (rlov.deleteAt isnull and voucher.deleteAt isnull) AND (loan.deleteAt isnull and rlo.deleteAt isnull)`)).
		WithArgs(validVoucherCode, inValidLenderID).
		WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(0))

	slaveMock.ExpectQuery(regexp.QuoteMeta(
		`SELECT count(*) FROM "voucher" JOIN r_loan_order_voucher as rlov ON rlov."voucherId" = voucher.id JOIN r_loan_order as rlo ON rlo."loanOrderId" = rlov."loanOrderId" JOIN loan ON loan.id = rlo."loanId" WHERE (voucher."voucherNo" = $1 and loan."investorId" = $2) AND (rlov.deleteAt isnull and voucher.deleteAt isnull) AND (loan.deleteAt isnull and rlo.deleteAt isnull)`)).
		WithArgs(invalidVoucherCode, validLenderID).
		WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(0))

	slaveMock.ExpectQuery(regexp.QuoteMeta(
		`SELECT count(*) FROM "voucher" JOIN r_loan_order_voucher as rlov ON rlov."voucherId" = voucher.id JOIN r_loan_order as rlo ON rlo."loanOrderId" = rlov."loanOrderId" JOIN loan ON loan.id = rlo."loanId" WHERE (voucher."voucherNo" = $1 and loan."investorId" = $2) AND (rlov.deleteAt isnull and voucher.deleteAt isnull) AND (loan.deleteAt isnull and rlo.deleteAt isnull)`)).
		WithArgs(invalidVoucherCode, inValidLenderID).
		WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(0))

	type args struct {
		lenderID    string
		voucherCode string
	}

	testCases := []struct {
		name string
		args args
		want int64
	}{
		{"valid Voucher", args{validLenderID, validVoucherCode}, 1},
		{"invalid Voucher", args{validLenderID, invalidVoucherCode}, 0},
		{"invalid LenderID", args{inValidLenderID, validVoucherCode}, 0},
		{"invalid LenderID", args{inValidLenderID, invalidVoucherCode}, 0},
	}

	repo := Repository{MasterDB: masterDB, SlaveDB: slaveDB}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			count, _ := repo.GetVoucherUsageCount(testCase.args.lenderID, testCase.args.voucherCode)
			if !reflect.DeepEqual(count, testCase.want) {
				t.Fatalf("repo.UpdateLoanStage() = %v, want %v", count, testCase.want)
			}
		})
	}
}
