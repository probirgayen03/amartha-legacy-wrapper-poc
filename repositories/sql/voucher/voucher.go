package voucher

import (
	"strings"

	voucherModel "bitbucket.org/probirgayen03/amartha-legacy-wrapper-poc/models/voucher"
	"github.com/jinzhu/gorm"
	log "github.com/sirupsen/logrus"
)

var tableName = "voucher"

type IRepository interface {
	GetVoucher(voucherCode string) (voucherModel.Voucher, error)
	GetVoucherUsageCount(customerID string, voucherCode string) (int64, error)
}

type Repository struct {
	MasterDB *gorm.DB
	SlaveDB  *gorm.DB
}

func GetRepository(masterDB, slaveDB *gorm.DB) IRepository {
	return &Repository{
		MasterDB: masterDB,
		SlaveDB:  slaveDB,
	}
}

func (repo Repository) GetVoucher(voucherCode string) (voucherModel.Voucher, error) {
	var voucher voucherModel.Voucher
	err := repo.SlaveDB.Table(tableName).
		Where(`"voucherNo" = ?`, strings.ToUpper(voucherCode)).
		First(&voucher).Error

	if err != nil {
		log.WithFields(log.Fields{
			"file": "voucher.go",
			"func": "GetVoucher",
		}).Error(err.Error())
	}

	return voucher, err
}

func (repo Repository) GetVoucherUsageCount(customerID string, voucherCode string) (int64, error) {
	var count int64
	err := repo.SlaveDB.Table(tableName).
		Joins(`JOIN r_loan_order_voucher as rlov ON rlov."voucherId" = voucher.id`).
		Joins(`JOIN r_loan_order  as rlo ON rlo."loanOrderId" = rlov."loanOrderId"`).
		Joins(`JOIN loan ON loan.id = rlo."loanId"`).
		Where(`voucher."voucherNo" = ? and loan."investorId" = ?`, voucherCode, customerID).
		Where(`rlov.deleteAt isnull and voucher.deleteAt isnull`).
		Where(`loan.deleteAt isnull and rlo.deleteAt isnull`).
		Count(&count).Error

	if err != nil {
		log.WithFields(log.Fields{
			"file": "voucher.go",
			"func": "GetVoucherUsageCount",
		}).Error(err.Error())
	}

	return count, err
}
