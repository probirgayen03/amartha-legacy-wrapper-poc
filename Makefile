all: run

run:
	go run main.go

dev:
	air
   
coverage:
	go test ./... -v -coverprofile coverage.out

docs: 
	swag init -g main.go -o docs

clean:
	go mod tidy

mock:
	mockgen -source=controllers/investor/investor_controller.go -destination=controllers/investor/mock/investor_controller_mockgen.go -package=mock
	mockgen -source=controllers/loan/loan_controller.go -destination=controllers/loan/mock/loan_controller_mockgen.go -package=mock
	# mockgen -source=controllers/payment/payment_controller.go -destination=controllers/payment/mock/payment_controller_mockgen.go -package=mock
	
	# service mocks
	mockgen -source=services/investor/investor_service.go -destination=services/investor/mock/investor_service_mockgen.go -package=mock
	mockgen -source=services/loan/loan_service.go -destination=services/loan/mock/loan_service_mockgen.go -package=mock
	mockgen -source=services/voucher/voucher_service.go -destination=services/voucher/mock/voucher_service_mockgen.go -package=mock
	# mockgen -source=services/payment/payment_service.go -destination=services/payment/mock/payment_service_mockgen.go -package=mock
	
	# repository mocks
	mockgen -source=repositories/sql/investor/investor.go -destination=repositories/sql/investor/mock/investor_mockgen.go -package=mock
	mockgen -source=repositories/sql/loan/loan.go -destination=repositories/sql/loan/mock/loan_mockgen.go -package=mock
	mockgen -source=repositories/sql/cif/cif.go -destination=repositories/sql/cif/mock/cif_mockgen.go -package=mock
	mockgen -source=repositories/sql/voucher/voucher.go -destination=repositories/sql/voucher/mock/voucher_mockgen.go -package=mock
 