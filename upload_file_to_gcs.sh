#!/bin/bash
SERVICE_ACCOUNT=$1
GCS_BUCKET=$2
SUFFIX=$3

# Authenticate to GCP
echo $SERVICE_ACCOUNT | base64 -d > ./amartha.json
gcloud auth activate-service-account --key-file ./amartha.json

VERSION=$(head -1 ./semantic_version.txt)

# Check if artifact has already been uploaded to GCS
gsutil -q stat gs://$GCS_BUCKET/legacy-wrapper-service-${VERSION}${SUFFIX}-*.rpm
RETURN_VALUE=$?

if [ $RETURN_VALUE == 0 ]; then
    echo "File already uploaded to GCS"
    gsutil list gs://$GCS_BUCKET/legacy-wrapper-service-${VERSION}${SUFFIX}-*.rpm > ./gcs_url.txt
    GCS_URL=$(head -1 ./gcs_url.txt)
    LONG_VERSION=(${GCS_URL//"gs://$GCS_BUCKET/"/ })
    echo $LONG_VERSION > ./version.txt
else
    echo "File not found in GCS"
    # Build artifact
    mkdir -p /root/rpmbuild/SOURCES/
    cp -a legacy-wrapper-service /root/rpmbuild/SOURCES/
    cp -a redhat-package/legacy-wrapper-service.service /root/rpmbuild/SOURCES
    rpmbuild --define "_version ${VERSION}${SUFFIX}" -bb ./redhat-package/legacy-wrapper-service.spec
    find /root/rpmbuild/RPMS/x86_64/ -name "*.rpm" -type f -mtime -1 -exec basename {} ';' 2>&1 | grep -v "Permission denied" > ./version.txt
    LONG_VERSION=$(head -1 ./version.txt)
    # Upload artifact to GCS
    gsutil cp /root/rpmbuild/RPMS/x86_64/$LONG_VERSION gs://$GCS_BUCKET/
fi

# Generate self-signed URL for the artifact in GCS (valid only for 2 minutes)
gsutil signurl -m GET -d 2m ./amartha.json gs://$GCS_BUCKET/$LONG_VERSION > ./self-signed-url.txt
