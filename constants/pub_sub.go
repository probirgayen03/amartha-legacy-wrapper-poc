package constants

// SUBSCRIPTIONS ...
// List of subscriptions
var SUBSCRIPTIONS = []string{
	"account-subscription",
	"loan-subscription",
}
